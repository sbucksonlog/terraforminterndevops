provider "aws" {
  region = "${var.env_region}"
}

#terraform {
#  required_version = "~> 0.12"
#}

# Query all avilable Availibility Zone
data "aws_availability_zones" "available" {}

#Zone ID
data "aws_elb_hosted_zone_id" "main" {}

module "vpc_networking" {
  source        = "./vpcNetworking"
  env_region    = "${var.env_region}"
  env_platform  = "${var.env_platform}"
  vpc_cidr      = "${var.vpc_cidr}"
  public_cidrs  = "${var.public_cidrs}"
  available_zone = "${data.aws_availability_zones.available.names}"
}

module "security_group" {
  source	= "./securityGroup"
  vpc_id 	= "${module.vpc_networking.vpc_id}"
  env_region    = "${var.env_region}"
  env_platform  = "${var.env_platform}"
  fw_in_intern = "${var.fw_in_intern}"
}

module "intern_host" {
  source         = "./ec2Instance"
  env_region     = "${var.env_region}"
  env_platform   = "${var.env_platform}"
  public_key     = "${var.intern_public_key}"
  aws_ami_id    = "${var.intern_aws_ami_id}"
  instance_type  = "${var.intern_instance_type}"
  instance_count = "1"
  instance_name = "${var.intern_instance_name}"
  security_group_id = "${module.security_group.security_group_intern}"
  subnet_id = "${module.vpc_networking.public_subnets}"
  available_zone = "${var.available_zone}"
  assign_eip = "true"
  volume_size = "${var.intern_volume_size}"
}
