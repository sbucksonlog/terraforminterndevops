provider "aws" {
  region = "${var.env_region}"
}

# Security Group Creation
resource "aws_security_group" "web" {
  name   = "${var.env_platform}_web"
  description = "Private Traffic"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group" "rds" {
  name   = "${var.env_platform}_rds"
  description = "Private Traffic"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group" "intern" {
  name   = "${var.env_platform}_intern"
  description = "public Traffic"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group" "elb" {
  name   = "${var.env_platform}_elb"
  description = "public Traffic"
  vpc_id = "${var.vpc_id}"
}

# Ingress Security Port intern 
resource "aws_security_group_rule" "intern_inbound_access" {
  count             = "${length(var.fw_in_intern)}"
  from_port         = "${var.fw_in_intern[count.index]}"
  protocol          = "tcp"
  security_group_id = "${aws_security_group.intern.id}"
  to_port           = "${var.fw_in_intern[count.index]}"
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

# OutBound Rule for intern
resource "aws_security_group_rule" "intern_outbound_access" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.intern.id}"
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}
