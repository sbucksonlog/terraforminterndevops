/* Networking Settings */
vpc_cidr = "10.0.0.0/16"
public_cidrs = ["10.0.1.0/24"]

/* AWS Region*/
env_region = "us-west-2"
env_platform = "intern"
available_zone = "us-west-2a"

/*Inbound Firewall Security groups*/
fw_in_intern = ["22","80"]

/*Intern host*/
intern_public_key="./config/intern-keys.pub"
intern_aws_ami_id="ami-06d51e91cea0dac8d"
intern_instance_type="t2.micro"
intern_instance_name="intern"
intern_volume_size="10"

/*Instance Common Settings */
aws_ami_id = "ami-06d51e91cea0dac8d"
instance_count = "1"
