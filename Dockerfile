FROM alpine:3.10

MAINTAINER anandbabu.tn@gmail.com 

# Latest version is here
# https://releases.hashicorp.com/terraform/
#ENV TERRAFORM_VERSION=0.12.29
ENV TERRAFORM_VERSION=1.0.11

# Initilize volume
VOLUME ["/data"]

# Set working directory
WORKDIR /data

# Install dependencies
#RUN apk update && \
#    apk add curl jq python bash ca-certificates git openssl unzip wget nodejs nodejs-npm 

# Intall Terraform
RUN wget -q https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin

#Removing Temp
RUN rm -rf /tmp/* && \
    rm -rf /var/cache/apk/* && \
    rm -rf /var/tmp/*

ENTRYPOINT ["/usr/bin/terraform"]
#CMD ["bash"]
