provider "aws" {
  region = "${var.env_region}"
}

# Query all avilable Availibility Zone
data "aws_availability_zones" "available" {}

resource "aws_key_pair" "auth" {
  key_name   = "${var.instance_name}.${var.env_platform}"
  public_key = "${file(var.public_key)}"
}

resource  "aws_eip" "public" {
  count = var.assign_eip ? 1 : 0
  instance = "${element(aws_instance.instance.*.id, count.index)}"
  vpc = true
}

resource "aws_instance" "instance" {
  count                  = "${var.instance_count}"
  ami                    = "${var.aws_ami_id}"
  instance_type          = "${var.instance_type}"
  key_name               = "${aws_key_pair.auth.id}"
  vpc_security_group_ids = ["${var.security_group_id}"]
  subnet_id              = "${element(var.subnet_id, count.index )}"
  user_data              = "${data.template_file.user-init.rendered}"
  
root_block_device {
  volume_size           = "${var.volume_size}"
}
  tags = {
    Name = "${var.instance_name}.${var.env_platform}.${count.index + 1}"
  }
}

data "template_file" "user-init" {
  template = "${file("${path.module}/userdata.tpl")}"
}
