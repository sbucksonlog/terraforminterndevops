#!/bin/bash
#Install tools
sudo apt update -y 
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common git -y 

#Install Docker
sudo curl https://get.docker.com | bash
sudo usermod -aG docker $USER

# Install Docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#Run Nginx
#First Scenario
wget -O paradigm.zip  https://html5up.net/paradigm-shift/download
unzip paradigm.zip -d /site-content/
sudo docker run -it --rm -d -p 80:80 -v /site-content/:/usr/share/nginx/html --name web nginx

#Second Scenario
#git clone https://github.com/pmckeetx/docker-nginx.git
#cd docker-nginx
#docker-compose up -d 
