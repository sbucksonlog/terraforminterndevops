variable "env_region" {}
variable "env_platform" {}
variable "public_key" {}
variable "aws_ami_id" {}
variable "instance_type" {}
variable "instance_count" {}
variable "instance_name" {}
variable "security_group_id" {}
variable "subnet_id" {}
variable "available_zone" {}
variable "volume_size" {}
variable "assign_eip" {
  description = "If set to true, assign ElasticIP"
  type        = bool
}

