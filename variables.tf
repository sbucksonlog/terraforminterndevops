variable "env_region" {
  description = "The region to launch"
  default     = ""
}
variable "env_platform" {
  description = "The CIDR block for the VPC"
  default     = ""
}
variable "vpc_cidr" {
  description = "The CIDR block for the VPC"
  default     = ""
}
variable "available_zone" {
  description = "A list of Availability zones in the region"
  default     = []
}

variable "public_cidrs" {
  description = "A list of public subnets inside the VPC."
  default     = []
  type 	      = list(string)
}

variable "fw_in_intern" {
        type = list(string)
}

variable "intern_public_key" {}
variable "intern_aws_ami_id" {}
variable "intern_instance_type" {}
variable "intern_instance_name" {}
variable "intern_volume_size" {}

variable "aws_ami_id" {}

variable "instance_count" {}
