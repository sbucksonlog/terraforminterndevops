variable "env_region" {
  description = "The region to launch"
  default     = ""
}
variable "env_platform" {
  description = "The CIDR block for the VPC"
  default     = ""
}
variable "vpc_cidr" {
  description = "The CIDR block for the VPC"
  default     = ""
}
variable "available_zone" {
  description = "A list of Availability zones in the region"
  default     = []
}

variable "public_cidrs" {
  description = "A list of public subnets inside the VPC."
  default     = []
  type = list(string)
}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  default     = "default"
}

variable "enable_dns_hostnames" {
  description = "should be true if you want to use private DNS within the VPC"
  default     = true
}

variable "enable_dns_support" {
  description = "should be true if you want to use private DNS within the VPC"
  default     = true
}

variable "enable_classiclink" {
  description = "should be true if you want to use ClassicLink within the VPC"
  default     = true
}

variable "enable_classiclink_dns_support" {
  description = "should be true if you want to use private DNS within the classiclinks"
  default     = true
}

variable "map_public_ip_on_launch" {
  description = "should be false if you do not want to auto-assign public IP on launch"
  default     = true
}

variable "public_cidrs_tags" {
  description = "Additional tags for the public subnets"
  default     = {}
}

variable "tags" {
  description = "A map of tags to add to all resources"
  default     = {}
}
