provider "aws" {
  region = "${var.env_region}"
}

resource "aws_vpc" "main" {
  cidr_block           = "${var.vpc_cidr}"
  instance_tenancy     = "${var.instance_tenancy}"
  enable_dns_hostnames = "${var.enable_dns_hostnames}"
  enable_dns_support   = "${var.enable_dns_support}"

  enable_classiclink              = "${var.enable_classiclink}"
  enable_classiclink_dns_support  = "${var.enable_classiclink_dns_support}"

  tags = merge(var.tags, {"Name" = format("%s", var.env_platform)})
}

# Creating Internet Gateway
resource "aws_internet_gateway" "gw" {
  count = "${length(var.public_cidrs) > 0 ? 1 : 0}"
  vpc_id = "${aws_vpc.main.id}"
  tags = merge(var.tags, tomap({"Name" = format("%s-igw", var.env_platform)}))
}

# Public Route Table
resource "aws_route_table" "public_route" {
  count = "${length(var.public_cidrs) > 0 ? 1 : 0}"

  vpc_id = "${aws_vpc.main.id}"
#  propagating_vgws = ["${var.public_propagating_vgws}"]
  tags = merge(var.tags, tomap({"Name" = format("%s-rt-public", var.env_platform)}))
}

resource "aws_route" "public_internet_gateway" {
  count = "${length(var.public_cidrs) > 0 ? 1 : 0}"

  route_table_id         = "${aws_route_table.public_route[count.index].id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gw[count.index].id}"
}

# Public Subnet
resource "aws_subnet" "public_subnet" {
  count = "${length(var.public_cidrs)}"

  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "${var.public_cidrs[count.index]}"
  availability_zone       = "${element(var.available_zone, count.index)}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"

  tags = merge(var.tags, var.public_cidrs_tags, tomap({"Name" = format("%s-subnet-public-%s", var.env_platform, element(var.available_zone, count.index))}))
}

resource "aws_route_table_association" "public" {
  count = "${length(var.public_cidrs)}"

  subnet_id      = "${element(aws_subnet.public_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.public_route[count.index].id}"
}
