output "public_subnets" {
  value = "${aws_subnet.public_subnet.*.id}"
}

output "vpc_id" {
  value = "${aws_vpc.main.id}"
}

output "vpc_cidr_block" {
  value = "${aws_vpc.main.cidr_block}"
}

output "public_route_table_ids" {
  value = ["${aws_route_table.public_route.*.id}"]
}

output "default_security_group_id" {
  value = "${aws_vpc.main.default_security_group_id}"
}

output "igw_id" {
  value = "${aws_internet_gateway.gw.*.id}"
}

output "default_network_acl_id" {
  value = "${aws_vpc.main.default_network_acl_id}"
}
